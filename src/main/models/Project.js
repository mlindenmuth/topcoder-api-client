export default class {
  constructor(id, name, description, billingAccountId) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.billingAccountId = billingAccountId;
  }
}
