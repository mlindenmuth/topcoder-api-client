export const ConfidentialityType = {
  Public: 'public',
  Private: 'standard_cca',
};

export const SubTrack = {
  ApplicationFrontEndDesign: 'APPLICATION_FRONT_END_DESIGN',
  Architecture: 'ARCHITECTURE',
  AssemblyCompetition: 'ASSEMBLY_COMPETITION',
  BugHunt: 'BUG_HUNT',
  Code: 'CODE',
  DesignFirst2Finish: 'DESIGN_FIRST_2_FINISH',
  First2Finish: 'FIRST_2_FINISH',
  IdeaGeneration: 'IDEA_GENERATION',
  MarathonMatch: 'MARATHON_MATCH',
  PrintOrPresentation: 'PRINT_OR_PRESENTATION',
  TestSuites: 'TEST_SUITES',
  UiProtoTypeCompetition: 'UI_PROTOTYPE_COMPETITION',
  WebDesigns: 'WEB_DESIGNS',
  WidgetOrMobileScreenDesign: 'WIDGET_OR_MOBILE_SCREEN_DESIGN',
  WireFrames: 'WIREFRAMES',
};

export const ReviewType = {
  Community: 'COMMUNITY',
  Internal: 'INTERNAL',
};

export const Status = {
  Deleted: 'DELETED',
  Draft: 'DRAFT',
}

export default class {
  constructor(
    id, name, projectId, subTrack, confidentialityType, reviewType,
    registrationStartsAt, submissionEndsAt, milestoneId, status,
  ) {
    this.id = id;
    this.name = name;
    this.projectId = projectId;
    this.subTrack = subTrack;
    this.confidentialityType = confidentialityType;
    this.reviewType = reviewType;
    this.registrationStartsAt = registrationStartsAt;
    this.submissionEndsAt = submissionEndsAt;
    this.milestoneId = milestoneId;
    this.status = status;
    this.detailedRequirements = '';
    this.submissionGuidelines = '';
    this.prizes = [];
    this.platforms = [];
    this.technologies = [];
    this.finalDeliverableTypes = [];
  }
}
