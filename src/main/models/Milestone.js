export const Status = {
  Completed: 'COMPLETED',
  Overdue: 'OVERDUE',
  Upcoming: 'UPCOMING',
};

export default class {
  constructor(id, name, description, status, dueDate) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.status = status;
    this.dueDate = dueDate;
  }
}
