import Endpoint from './Endpoint';
import * as models from './../models';

export default class extends Endpoint {
  async get(id) {
    const response = await super.get(`/direct/projects/${id}`);
    const { content } = response.result;
    const { project } = content;
    const { billingAccountIds } = content;

    const billingAccountId = billingAccountIds &&
    billingAccountIds.length > 0 ? billingAccountIds[0] : null;

    return new models.Project(
      project.projectId,
      project.name, project.description, billingAccountId,
    );
  }

  async create(project) {
    const response = await super.post('/direct/projects', {
      projectName: project.name,
      projectDescription: project.description,
      billingAccountId: project.billingAccountId || 0,
    });
    return new models.Project(response.result.content.projectId);
  }
}
