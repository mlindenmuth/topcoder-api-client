import request from 'request-promise-native';

export default class {
  constructor(host, authorization) {
    this.host = host;
    this.authorization = authorization;
  }

  async get(path, skipAuthorization) {
    return request.get(this.host + path, {
      json: true,
      headers: await this.getHeaders(skipAuthorization),
    });
  }

  async post(path, body, skipAuthorization) {
    return request.post(this.host + path, {
      json: true,
      headers: await this.getHeaders(skipAuthorization),
      body,
    });
  }

  async put(path, body, skipAuthorization) {
    return request.put(this.host + path, {
      json: true,
      headers: await this.getHeaders(skipAuthorization),
      body,
    });
  }

  async getHeaders(skipAuthorization) {
    const headers = {};

    if (!skipAuthorization) {
      const token = await this.authorization.getToken();
      headers.Authorization = `Bearer ${token.token}`;
    }

    return headers;
  }
}
