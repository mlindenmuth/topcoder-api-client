import Endpoint from './Endpoint';
import * as models from './../models';

export default class extends Endpoint {
  async get(id) {
    const response = await super.get(`/challenges/?filter=id=${id}`);
    const { metadata } = response.result;
    const { content } = response.result;

    if (metadata.totalCount > 0) {
      const challenge = content[0];

      return new models.Challenge(
        challenge.id,
        challenge.name,
        challenge.projectId,
        challenge.subTrack,
        challenge.isPrivate ?
          models.ConfidentialityType.Private : models.ConfidentialityType.Public,
        challenge.reviewType,
        new Date(challenge.registrationStartDate),
        new Date(challenge.submissionEndDate),
        null,
        challenge.status,
      );
    }

    throw new Error('no results');
  }

  async create(challenge) {
    const response = await super.post('/challenges', {
      param: {
        projectId: challenge.projectId,
        name: challenge.name,
        subTrack: challenge.subTrack,
        confidentialityType: challenge.confidentialityType,
        reviewType: challenge.reviewType,
        registrationStartsAt: challenge.registrationStartsAt,
        registrationEndsAt: challenge.submissionEndsAt,
        submissionEndsAt: challenge.submissionEndsAt,
        milestoneId: challenge.milestoneId || 1,
        detailedRequirements: challenge.detailedRequirements,
        submissionGuidelines: challenge.submissionGuidelines,
        prizes: challenge.prizes,
        platforms: challenge.platforms,
        technologies: challenge.technologies,
        finalDeliverableTypes: challenge.finalDeliverableTypes,
      },
    });

    const { content } = response.result;
    return new models.Challenge(
      content.id,
      content.name,
      content.projectId,
      content.subTrack,
      content.confidentialityType,
      content.reviewType,
      new Date(content.registrationStartsAt),
      new Date(content.submissionEndsAt),
      content.milestoneId,
    );
  }

  async update(challenge) {
    const c = Object.assign({}, challenge);
    delete c.status;

    const response = await super.put(`/challenges/${challenge.id}`, {
      param: c,
    });
    return response.result.content;
  }
}
