import Endpoint from './Endpoint';
import * as models from './../models';

export default class extends Endpoint {
  async create(projectId, milestone) {
    const response = await super.post(`/direct/projects/${projectId}/milestones`, {
      name: milestone.name,
      description: milestone.description,
      status: milestone.status,
      dueDate: milestone.dueDate,
    });
    const { content } = response.result;
    return new models.Milestone(content);
  }
}
