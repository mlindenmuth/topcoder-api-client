import Endpoint from './Endpoint';

export default class extends Endpoint {
  async create(externalToken, refreshToken) {
    const response = await this.post('/authorizations', {
      param: {
        externalToken,
        refreshToken,
      },
    }, true);

    return response.result.content;
  }
}
