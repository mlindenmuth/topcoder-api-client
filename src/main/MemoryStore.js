export default class {
  constructor() {
    this.data = {};
  }

  async set(key, value) {
    this.data[key] = value;
    return Promise.resolve(this.data[key]);
  }

  async get(key) {
    return Promise.resolve(this.data[key]);
  }

  async clear() {
    this.data = {};
    return Promise.resolve();
  }
}
