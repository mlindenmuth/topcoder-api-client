export default class {
  constructor(auth0, authorizations, store) {
    this.auth0 = auth0;
    this.authorizations = authorizations;
    this.store = store;
  }

  async getToken() {
    const auth0Token = await this.auth0.getToken();
    let v3Token = await this.store.get('v3_token');

    if (!v3Token || this.constructor.isExpired(v3Token)) {
      v3Token = await this.authorizations.create(auth0Token.idToken, auth0Token.refreshToken);
      v3Token = await this.store.set('v3_token', v3Token);
    }

    return v3Token;
  }

  static isExpired(token) {
    return new Date(JSON.parse(Buffer.from(token.token.split('.')[1], 'base64').toString('ascii')).exp * 1000) < new Date();
  }
}
