export default class {
  constructor(authentication, username, password, store) {
    this.authentication = authentication;
    this.username = username;
    this.password = password;
    this.store = store;
  }

  async getToken() {
    let token = await this.store.get('auth0_token');

    if (!token) {
      return new Promise((resolve, reject) => {
        this.authentication.loginWithResourceOwner({
          username: this.username,
          password: this.password,
          connection: 'LDAP',
          scope: 'openid profile offline_access',
          device: 'Browser',
        }, (err, result) => {
          if (err) {
            reject(err);
          } else {
            token = this.store.set('auth0_token', result);
            resolve(token);
          }
        });
      });
    }
    return token;
  }
}
