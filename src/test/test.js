import assert from 'assert';
import { describe, it } from 'mocha';
import { Authentication } from 'auth0-js';

import { Auth0, Authorization, MemoryStore, api, models } from './../main';

const apiHost = process.env.API_HOST;
const auth0Host = process.env.AUTH0_HOST;
const auth0ClientId = process.env.AUTH0_CLIENT_ID;
const auth0Username = process.env.AUTH0_USERNAME;
const auth0Password = process.env.AUTH0_PASSWORD;

const store = new MemoryStore();
const authentication = new Authentication({ domain: auth0Host, clientID: auth0ClientId });
const auth0 = new Auth0(authentication, auth0Username, auth0Password, store);

const authorizations = new api.Authorizations(apiHost);
const authorization = new Authorization(auth0, authorizations, store);
const projects = new api.Projects(apiHost, authorization);
const challenges = new api.Challenges(apiHost, authorization);
const milestones = new api.Milestones(apiHost, authorization);

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

describe('Auth0', () => {
  describe('getToken', () => {
    let firstToken;

    it('should return a token with valid credentials', async () => {
      const token = await auth0.getToken();
      firstToken = token;
      assert.ok(token);
    });

    it('should store a token', async () => {
      const token = await auth0.getToken();
      assert.ok(token);
      assert.ok(token === firstToken);
    });
  });
});


describe('Authorization', () => {
  describe('getToken', () => {
    let firstToken;

    it('should exchange an auto0 token for a v3 token', async () => {
      const token = await authorization.getToken();
      firstToken = token;
      assert.ok(token);
    });

    it('should store a token', async () => {
      const token = await authorization.getToken();
      assert.ok(token);
      assert.ok(token === firstToken);
    });
  });
});

describe('api', () => {
  let projectId;
  let milestoneId;

  describe('Projects', () => {
    it('should create a project', async () => {
      const project = await projects.create(new models.Project(null, 'API Client Test', 'Api client test.'));
      assert.ok(project);
      projectId = project.id;
    });

    it('should get a project', async () => {
      const project = await projects.get(projectId);
      assert.ok(project);
      assert.equal(project.id, projectId);
    });
  });


  describe('Milestones', () => {
    it('should create a milestone', async () => {
      const milestone = await milestones.create(projectId, new models.Milestone(null, 'Test Milestone', 'description', models.MilestoneStatus.Upcoming, new Date()));
      assert.ok(milestone);

      milestoneId = milestone.id;
    });
  });

  describe('Challenges', () => {
    let challenge;

    it('should create a challenge with a milestone', async () => {
      const start = new Date();
      const end = new Date();
      end.setDate(end.getDate() + 5);

      challenge = new models.Challenge(null, 'Test Challenge', projectId, models.SubTrack.Code, models.ConfidentialityType.Public, models.ReviewType.Community, start, end, milestoneId);
      challenge = await challenges.create(challenge);

      await sleep(60000);

      assert.ok(challenge);
    }).timeout(80000);

    it('should get a challenge', async () => {
      challenge = await challenges.get(challenge.id);
      assert.ok(challenge);
    });

    it('should update a challenge', async () => {
      challenge.name = 'Test Challenge (Update)';
      challenge = await challenges.update(challenge);
      assert.ok(challenge);
    });
  });
});
