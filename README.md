# Usage

## Store

Use the built-in memory store or create your own (eg redis) to store OAuth tokens.

```
import { MemoryStore } from 'topcoder-api-client';
const store = new MemoryStore();
``` 

## Authentication

Authenticating with the API requires first getting an OAuth token from auth0 then using the API itself to exchange that token for another token.

1. Create an [auth0-js](https://www.npmjs.com/package/auth0-js) authentication object configured with the auth0 domain and client id.
   ```
   import { Authentication } from 'auth0-js';
   const authentication = new Authentication({ domain: 'some host', clientID: 'some client id' });
   ```

1. Create an `Auth0` object to handle username/password authentication using the authentication object and store.
   ```
   import { Auth0 } from 'topcoder-api-client';
   const auth0 = new Auth0(authentication, 'some username', 'some password', store);
   ``` 
   
1. Create the Authorizations API controller used to swap tokens:
   ```
   import { api } from 'topcoder-api-client';
   const authorizations = new api.Authorizations('some api host');
   ```
   
1. Create the Authorization object that will get passed to every other API controller using the Auth0 and Authorizations controller:
   ```
   import { Authorization } from 'topcoder-api-client';
   const authorization = new Authorization(auth0, authorizations, store);
   ```
   
   Every request from any other API controller will use this object to get a token to add as an `Authorization: Bearer {token}` header. If the store does not contain a token or the token is expired it will attempt to get a new one before proceeding.

### Full Sample

```
import { Authentication } from 'auth0-js';

import { Auth0, Authorization, MemoryStore, api } from 'topcoder-api-client';

const store = new MemoryStore();
const authentication = new Authentication({ domain: 'some host', clientID: 'some client id' });
const auth0 = new Auth0(authentication, 'some username', 'some password', store);

const authorizations = new api.Authorizations('some api host');
const authorization = new Authorization(auth0, authorizations, store);
```


## Projects

```
// using authorization from above example
// create API controller
const projects = new api.Projects('some api host', authorization);
```

### Get Project

```
const projectA = await projects.get(someProjectId);
const projectB = await projects.get(anotherProjectId);
```

### Create Project

```
// billingAccountId is optional and defaults to 0
const project = await projects.create('some name', 'some description', billingAccountId);
```

## Milestones

```
// using authorization from above example
// create API controller
const milestones = new api.Milestones('some api host', authorization);
```

### Create Milestone

```
// status can be any value from api.MilestoneStatus (eg api.MilestoneStatus.Upcoming)
// dueDate is a standard javascript Date object
const milestone = await milestones.create(someProjectId, 'some name', 'some description', status, dueDate);
```

## Challenges

```
// using authorization from above example
// create API controller
const challenges = new api.Challenges('some api host', authorization);
```

### Get Challenge

```
const challengeA = await challenges.get(someChallengeId);
const challengeB = await challenges.get(anotherChallengeId);
```

### Create Challenge

```
// subTrack can be any value from api.ChallengeSubTrack (eg api.ChallengeSubTrack.Code)
// confidentialityType can be any value from api.ChallengeConfidentialityType (eg api.ChallengeConfidentialityType.Public)
// reviewType can be any value from api.ChallengeReviewType (eg api.ChallengeReviewType.Community)
// registrationStartsAt and submissionEndsAt are standard javascript Date objects
// milestoneId is optional and defaults to 0
const challenge = await challenges.create(someProjectId, 'some name', subTrack, confidentialityType, reviewType, registrationStartsAt, submissionEndsAt, milestoneId);
```

### Update Challenge

```
let challenge = await projects.update(challenge);
```